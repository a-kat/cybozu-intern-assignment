#!/usr/bin/env bash

#################################
TESTDATAPATH='testdata100l'
#################################

unset tmpdumpsh
unset tmpdumpgo

tmpdumpsh=`mktemp`
tmpdumpgo=`mktemp`

atexit() {
  [[ -n ${tmpdumpsh-} ]] && rm $tmpdumpsh
  [[ -n ${tmpdumpgo-} ]] && rm $tmpdumpgo
}

trap atexit EXIT
trap 'rc=$?; trap - EXIT; atexit; exit $?' INT PIPE TERM

cat $TESTDATAPATH | while read line
do
    echo -n $line | sha256sum | awk '{print $1}' >> $tmpdumpsh
done

go run main.go $TESTDATAPATH > $tmpdumpgo

diffdata=`diff $tmpdumpsh $tmpdumpgo`

if [ -z $diffdata ]; then
    echo OK
else
    echo failed
fi
