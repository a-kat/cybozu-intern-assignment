# cybozu-intern-assignment

Tool to asynchronously calculate the sha256 checksum of all lines of a file.

## usage

```bash
go run main.go <filepath> # sha256 checksum to stdout
```

## Test

```bash
./test.sh
```

If you want to use a different test data, change the TESTDATAPATH variable in test.sh.
