package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
)

func main() {

	flag.Parse()
	args := flag.Args()
	if len(args) != 1 {
		log.Fatalln("Invalid argument")
	}

	fileName := args[0]
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	var numOfLines uint
	var textLines []string

	scanner := bufio.NewScanner(f)

	// Count the number of lines and determine the size of the dump slice.
	for scanner.Scan() {
		textLines = append(textLines, scanner.Text())
		numOfLines++
	}
	dump := make([]string, numOfLines)

	wg := sync.WaitGroup{}
	for lines, lineText := range textLines {
		wg.Add(1)
		go func(lines int, lineText string) {
			defer wg.Done()
			checksum := sha256.Sum256([]byte(lineText))
			dump[lines] = strings.ToLower(hex.EncodeToString(checksum[:]))
		}(lines, lineText)
	}
	wg.Wait()

	w := bufio.NewWriter(os.Stdout)
	for _, dumpLine := range dump {
		fmt.Fprintln(w, dumpLine)
	}
	if err := w.Flush(); err != nil {
		log.Fatalln(err)
	}
}
